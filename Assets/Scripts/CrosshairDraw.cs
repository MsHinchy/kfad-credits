﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairDraw : MonoBehaviour
{
	Transform plane;

	public Material material;

	CreditsManager manager;

	float scale;

	bool canScaleUpYet = false;

	public static bool started, finished;

    int qualitySetting;

    void Awake()
	{
		started = false;
		finished = false;

		scale = 0.0f;
		canScaleUpYet = false;

        qualitySetting = 0;
    }

    void Start()
    {
        /*
        for (int i = 0; i < QualitySettings.names.Length; i++)
        {
            Debug.Log("names[" + i.ToString() + "] == " + QualitySettings.names[i]);
        }
        */

#if UNITY_EDITOR
#else
        qualitySetting = QualitySettings.GetQualityLevel();
#endif

        Cursor.visible = (qualitySetting == 1);

        scale = 0.0f;
        canScaleUpYet = false;

        manager = CreditsManager.Locate();

        if (qualitySetting == 0)
        {
            plane = GameObject.CreatePrimitive(PrimitiveType.Plane).transform;
            plane.gameObject.layer = 5;
            plane.name = "Crosshair";

            Destroy(plane.GetComponent<Collider>());

            plane.GetComponent<Renderer>().material = material;

            plane.parent = transform;
            plane.localPosition = new Vector3(0.0f, 0.0f, -10.0f);
            plane.localScale = new Vector3(0.0f, 1.0f, 0.0f);
            plane.localEulerAngles = new Vector3(-90.0f, 0.0f, 0.0f);
        }
        else
        {
            plane = null;
        }
	}

	void Update()
	{
		if (plane == null) return;

		if (finished)
		{
			plane.localPosition = manager.mousePositionCorrected;

			if (scale > 0.0f)
			{
				scale -= Time.smoothDeltaTime * 60.0f;
				plane.localScale = new Vector3(scale, 1.0f, scale);
				if (scale <= 0.0f)
				{
					plane.localPosition = Vector3.one * 100000.0f;
					Destroy(plane.gameObject);
					plane = null;
				}
			}
		}
		else if (started)
		{
			if (manager.mouseDown && (scale > 8.0f))
			{
				scale = 8.0f;
                Cursor.visible = (qualitySetting == 1);
            }
			
			if (scale < 10.0f)
			{
				if (!canScaleUpYet)
				{
					if (Time.smoothDeltaTime < 0.05f) canScaleUpYet = true;
				}
				else
				{
					scale += (10.0f - scale) * Time.smoothDeltaTime * 10.0f;
					plane.localScale = new Vector3(scale, 1.0f, scale);
				}
			}

			plane.localPosition = manager.mousePositionCorrected;
		}
	}
}
