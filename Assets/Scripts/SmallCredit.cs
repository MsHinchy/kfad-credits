﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SmallCredit : Credit
{
	public int startingHP = 1;

	override public void Start()
	{
		base.Start();

		hp = startingHP;
		scoreValue = startingHP;

		shakeLength = 0.15f;
		shakeIntensity = 32.0f;
	}
}
